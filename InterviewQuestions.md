# 1. Interview Questions

## 1.1 Java Questions
<details><summary>Java</summary>

### 1.1.1 Q1 What do you know about checked and unchecked exceptions?

<details><summary>Answer</summary>

> **Checked Exceptions**
> 
> Checked Exceptions are exceptions that are subclasses of throwable. So any exception that you throw in your code. 

> **Unchecked Exceptions**
> 
> Unchecked exceptions are under Error and RuntimeException. Which are generally something you cannot handle/ recover from. 
> Error and runtime exceptions are generally exceptions you try to prevent instead of handling as it would not be possible to
> for example outofmemoryerror if you run out of memory you can’t recover from it.

> **Null Pointer Exceptions**
> 
> Null pointer exception is a run time exception. It is not a throwable as then every method would need to declare 
> that they throw null pointer exception. 
		
</details>


### 1.1.2 Q2 What is a heap and stack in Java?

**Q2 What is a heap and stack in Java?**

<details><summary>Answer</summary>

> **Heap**
> 
> Java Heap space is used by the JRE to allocate memory to objects and classes. 
> Whenever we create an object it is always created in the heap space. 
> Garbage collection runs on the heap memory to free memory by removing objects that are no longer referenced in our code.  
> Any object created in the heap space has global access and can be referenced from anywhere in the application.
>

> **Stack**
> 
> Java Stack memory is used for execution of a thread. They contain the order in which methods are called in your program and 
> the objects they reference that are on the heap.  Stack memory is referenced lifo last in first out. 
> When a method is invoked a new 
> block is added to the stack and holds the local primitive values and references to other objects in the method. 

</details>

### 1.1.3 Question 3

**Q3 Explain the Garbage collector?**
<details><summary>Answer</summary>

> The garbage collector executes in a separate thread to that of the main program. 
> When objects on the heap no longer
> have a reference to it from the stack then the memory resource is released the object no longer exists. 

</details>

### 1.1.4 Question 4

**Q4 What is an Interface? Why would you use interfaces in the first place?**

<details><summary>Answer</summary>

> An interface defines a set of behaviors that must be implemented by the class that implements the 
> interface. An interface contains a set of abstract methods that must be implemented by a 
> class that implements the interface.
> 
> Because Java does not allow multiple inheritance, Creating a parent class in order to share a
> set of behaviors may not be possible as a child class may already have inherited from a different 
> parent class. 
> 
> And Using a parent class may mean other child classes may be forced to support 
> the parents behaviors even though it does not make sense to do so. 
> 
> Also In order for 2 classes to share a set of behaviors you would have to pick a parent common to both and that class would
> then pollute all other children classes as it would not make sense for those other child classes
> to support that behavior also.

</details>

### 1.1.5 Question 5

**Q5 Explain Design Patterns that you use?**
<details><summary>Answer</summary>

> **Dependency Injection**
> 
> Dependency Injection is used to decouple a class from its implementation. 
> When a new object is created using the new keyword it could be considered an anti-pattern as we are coupling a specific implementation of a class in our code.
> 
> A simple example of dependency injection is assigning an object instance to a variable of that type through a setter or constructor instead of using the new keyword. 
> 
> This is better because we can now pass in a subclass of the reference type or if the reference type is an interface any class that implements the interface can be assigned. 
> That way our code is more versatile and user’s of the code have more freedom. 
> 
> Frameworks such as the Spring framework use dependency injection that injects newly created objects specified in configuration. 
> So instead of hardcoding the creation of a new object in several places we can specify the object in one place.
> 
> The spring framework uses dependency injection to implement inversion of control. By calling our code instead of the other way around.

</details>



### 1.1.6 Question 6

**Q6 Describe what you know about Big O notation?**
<details><summary>Answer</summary>

> **Big O notation**
> 
> The majority of collections are O(1) or O(n) (linear) including linked list. Only trees are O(log n)
> 
> **O(1)** - getting value from a variable accessing value from a hash map given its key
> 
> **O(log n)** a for loop that skips elements in the for loop?
> 
> **O(n)** something that grows linearly, looping through 10 elements takes 10 times longer than 1 element
> 
> **O(n log n)** a for loop with an inner for loop where outer is  O(n) and inner is O(log n)
> 
> **O(n^p)** polynomial can be quadratic - 2, cubic - 3 etc  double for loop where both it’s loops is O(n) is a quadratic
> 
> **O(n!)** factorial algorithms   for (int i = 1; i <= factorial(n); i++)

</details>


### 1.1.7 Question 7

**Q7 HashTable vs HashMap**
<details><summary>Answer</summary>

> **HashTable vs HashMap**
> 
> HashTable is synchronized whereas HashMap is not. This makes HashMap better for non-threaded applications.
> HashTable does not allow null keys or values. HashMap allows one null key and many null values. 
> If you needed predictable ordering you could use LinkedHashMap a subclass of HashMap not an option for HashTable

</details>



### 1.1.8 Question 8

**Q8 What is the difference Between primitives and wrapper classes?**
<details><summary>Answer</summary>

> **Primitives** are predefined data types in java.
> **Wrappers** are used to convert primitives to objects and objects to primitives.
> Generic classes only work with objects and not primitives.

</details>


### 1.1.9 Question 9

**Q9 Explain Lambda’s in Java?**

<details><summary>Answer</summary>

> A function that does not belong to a class. and can be passed in as a parameter

</details>

### 1.1.10 Question 10

**Q10 Explain the difference between the Java clauses: "final", "finalize", and "finally"**
<details><summary>Answer</summary>

> **Final** restricts modification. variables cannot be changed, classes cannot be inherited, methods cannot be overridden
> **Finally** Used in cleaning up resources after try catch block
> **Finalize** is deprecated. is the method executed just before the object is destroyed

</details>


### 1.1.11 Question 11

**Q11 What are the 4 principles of object oriented languages?**
<details><summary>Answer</summary>

> **Polymorphism**
> 
> Given a reference to an interface or parent class When a method is called then a different implementation is executed 
> depending on the object

> **Inheritance**
> 
> Inheritance is used to create more specific implementation. A child class will have all the properties of the parent
> class

> **Abstraction**
> 
> Abstraction is about hiding complexity. Grouping state and functionality in a class that makes more understandable code.

> **Encapsulation**
> 
> Hiding the state of an object behind accessor methods is an example of encapsulation. Defining how internal state can be accessed

</details>



### 1.1.12 Question 12

**Q12 Explain Singleton, Observer, Factory, Abstract Factory and give examples?**
<details><summary>Answer</summary>

[Design Patterns](https://refactoring.guru/design-patterns/java)

**Answer**

> **Creational Pattern**
> 
> Specifies object creation mechanisms, which increase flexibility and reuse of existing code
> 
> **Structural Pattern**
> 
> Assemble objects and classes into larger structures, while keeping these structures flexible and efficient
> 
> **Behavioral Pattern**
> 
> About Algorithms and assignment of responsibilities between objects

> **Singleton**
> 
> A reference to the same object is returned and reused instead of making a new one.


> **Observer**
> 
> A subscription mechanism that notifies observers of an event


> **Factory**
>
> Interface for creating objects. Instead of using new keyword call a factory method that hides the complexity of
> creating an object.


> **Abstract Factory**
> 
> A family of factory methods. for example when you use a factory method to generate a 2s shape of a square. 
> Then to generate a 3d shape you generate a cube. 

</details>



### 1.1.13 Question 13

**Q13 Difference between iterator and enumerator?**
<details><summary>Answer</summary>

> **Enumerator**
> 
> Enumerator is older and you cannot modify the collection while traversing the collection using iteration.
> so the remove method does NOT exist. Only used by legacy classes like Vector, HashTable and Stack

> **iterator**
> 
> Always preferred over Enumerator. and shorter method names. 

</details>



### 1.1.14  Question 14

**Q14 How to build locally?**
<details><summary>Answer</summary>


> Maven and Gradle. Using Run Configuration in you IDE.

</details>



### 1.1.15 Question 15

**Q15 What is a Comparator?**
<details><summary>Answer</summary>

>
> A way to compare objects in a collection. An example of its use is in sorting a collection.
> where you pass in a comparator to specify how the sorting occurs. A comparator is a lambda that returns 
> -1 for less than, 0 for equals and 1 for greater than

</details>



### 1.1.16 Question 16

**Q16 Differences between Interfaces and Abstract classes?**
<details><summary>Answer</summary>

> **Interfaces**
> 
> Defines Behaviours that needs to be implemented by class that implements the interface. A class can implement 
> many interfaces

> **Abstract classes**
> 
> A Class that forces subclasses to implement any methods marked by the abstract keyword.
> A child class can only inherit from one parent class

</details>


### 1.1.17 Question 17

**Q17 Why use Interfaces?**
<details><summary>Answer</summary>

> Programming to an interface instead of to a class we can decouple code from its implementation.
> If your program has a reference to an interface we can swap out different implementation class that implement that interface.
> Inheritance does not allow this as easy as you cannot have multiple inheritance
> And also testing is easy as you can swap out a heavy complex class with a different class that implements the 
> interface and returns mock data. 

</details>

### 1.1.18 Question 18

**Q18 Describe how to use Mockito?**
<details><summary>Answer</summary>

> Mockito is used to return what is returned from calls to methods in dependencies.
> eg. mockito.when(this method is called).thenReturn(The data that is returned)

</details>


### 1.1.19 Question 19

**Q19 Difference between comparable and comparator?**
<details><summary>Answer</summary>

> **Comparable** Is directly implemented in the class, while comparator is passed in through as a parameter
> Use comparator when the objects being compared do not have a natural ordering.
</details>


### 1.1.20 Question 20

**Q20 Vector vs ArrayList?**
<details><summary>Answer</summary>

> **Vector**
> 
> Vector is like an arrayList. it is old and not used anymore. It is slower and threadsafe

</details>


### 1.1.21 Question 21

**Q21 What is Transient keyword?**
<details><summary>Answer</summary>

> **Transient**
>
> Transient is used in serialization. The variable marked as transient will not be serialised

</details>






### 1.1.24 Question 24

**Q24 What is an Optional?**
<details><summary>Answer</summary>

> Optional is a class that represents a value that may or may not be present. It was introduced in Java 8 to avoid NullPointerExceptions
>

</details>

### 1.1.25 Question 25
**Q25 What happens if we use Object as a key in Hashmap?**
<details><summary>Answer</summary>

> You can use an object as a key in hashmap as long as you implement the hashCode and equals methods in the objects class.
>

</details>

### 1.1.26 Question 26
**Q26 hashCode vs equals methods?**
<details><summary>Answer</summary>

> equals method is used to compare objects in a collection. and returns a boolean
> hashCode returns an integer. returns a hash of the object.
>

</details>

### 1.1.27 Question 27
**Q27 How to make a Class Immutable**
<details><summary>Answer</summary>

>

</details>

### 1.1.28 Question 28
**Q28 Explain Rest request params in Spring? And Mappings**
<details><summary>Answer</summary>

> @RequestParam vs @PathVariable Annotations
> @PathVariable annotation extracts value from url
> 
> @RequestParam annotation extracts value from after the question mark

</details>


### 1.1.29 Question 29
**Q29 Upcasting and Downcasting**
<details><summary>Answer</summary>

> Upcasting is assigning a child object to a reference of its parent.
> Downcasting is assigning a parent object to a reference of its child and requires explicit typecasting (Child reference)
>

</details>


### 1.1.30 Question 30
**Q30  Array vs ArrayList**
<details><summary>Answer</summary>

> **Array**
> Fixed length. for primitives 
> int arr[] = new int[10];
> 
> **ArrayList**
> variable length. Generics. Cannot use primitives in ArrayList Type
> Arraylist<Type> al = new ArrayList<Type>();


</details>



### 1.1.31 Question 31
**Q31 Duplicate String in an array**
<details><summary>Answer</summary>

>
</details>



### 1.1.32 Question 32
**Q32 Reverse a String**
<details><summary>Answer</summary>

>
</details>


### 1.1.33 Question 33
**Q33 How to make a singleton thread safe?**
<details><summary>Answer</summary>

> Use the synchronized keyword on the method definition to make method thread safe. Or if that is poor performance inside factory method declare synchronized(this) block just the object instantiation part if object does not already exist. if object already exists returning it does not require to be thread safe. And use volatile keyword on object definiton.
> 
```
class Foo {
    private volatile Helper helper;

    public Helper getHelper() {
        Helper localRef = helper;
        if (localRef == null) {
            synchronized (this) {
                localRef = helper;
                if (localRef == null) {
                    helper = localRef = new Helper();
                }
            }
        }
        return localRef;
    }

    // other functions and members...
}
```
</details>

### 1.1.34 Question 34
**Q34 What is the volatile keyword**
<details><summary>Answer</summary>

> Changes made to a variable in one thread is immediately visible in all threads. This means the variable is read from primary memory and not cached.
</details>


### 1.1.35 Question 35
**Q35 How to make accessing collections thread safe?**
<details><summary>Answer</summary>

> We can use the static synchronization wrappers from the collections class.
> Collections.synchronizedCollection returns a thread safe collection.
> 
> Concurrent collections allow multiple threads access to collections at same time as they are assessing different parts of collection. That means they are faster. eg  ConcurrentHashMap

</details>

### 1.1.36 Question 36
**Q36 Explain Solid Principles?**
<details><summary>Answer</summary>

> Single Responsibility
> A class should have only one responsibility. 
> 
> Open/Closed
> Open for extension and closed for modification. You should not need to modify existing code that calls your class if you change it.
> 
> Liskov substitution
> If a child class is a child of a parent class then we can replace the parent class with the child class without disrupting the behaviour of out program.
> 
> Interface Segregation
> Larger interfaces should be split into smaller ones for simplicity and implementing class only need be concerned with what is relevant to them.
> 
> Dependency Inversion
> Dependencies in out class should be assigned values using constructors or setters. This allows users of out class to define their own implementation. This reduces the coupling in our class as we are no longer defining out dependencies.

</details>

</details>



## 1.2 Spring Questions

<details><summary>Spring Questions</summary>

**Q1 What is the Difference between RestController and Controller annotation?**

<details><summary>Answer</summary>

> RestController annotation is a combination of Controller and ResponseBody annotations.
> Therefore the restcontroller automatically serializes return objects into HttpResponse.

</details>


**Q2 How many scopes of spring is used?**

<details><summary>Answer</summary>

> **Singleton Scope** 
> 
> You can specify a bean as a singleton scope with the @scope annotation. A singleton scoped bean are scoped to single application context only. This is the default scope of the bean.

> **Prototype Scope**
> 
> Will return a new instance every time it is requested.


> **Request Scope**
> 
> A new instance is returned each time. 
> Even if we set the value of an attribute on the object when we make another request it is now the original value.


> **Session Scope**
> 
> For the current session if we make a request of a session scoped object then it will return the same instance for the lifetime of the session.

> **Application Scope**
> 
> Similar to Singleton scope. But is scoped to the servlet context. 
> An instance of an application scoped bean is shared among multiple servlet-based applications running the same servlet context.

> **Websocket Scope**
> 
> The same instance of the bean is returned whenever that bean is accessed during the lifetime of the websocket session.

</details>

**Q3 What is a spring bean?**

<details><summary>Answer</summary>

> Any objects that are handled by the Spring framework through dependency injection and inversion of control is a spring bean. 
> A pojo or model would not be a spring bean.

</details>


**Q4 What is the difference between Component annotation and repository, Controller and service annotations?**

<details><summary>Answer</summary>

> **Component annotation** 
> 
> The component annotation is used by spring to recognise the class as a generic bean to be registered by spring’s application context.

> **Repository annotation**
>
> Represents the database layer of application. Using this annotation is better than using the component annotation
> as it has automatic persistence exception translation enabled. This means low level  errors are converted into higher level spring exceptions.

> **Service annotation**
>
> This contains the business logic of the application. There is currently no difference between service and component unlike repository. 
> But this could be changed in the future. But the service annotation is more readable and explains what the class represents better.

> **Controller annotation**
> 
> Tells spring that this class serves as a controller in the application.

</details>


**Q5 What is component scanning in Spring?**


<details><summary>Answer</summary>

> If component scanning is enabled then Spring will scan the project for beans to be injected. 
> There is a componentScan annotation that is used on the applications configuration class that is used to tell spring which packages to scan as part of the component scan process.

</details>

**Q6 What is the difference between Bean annotation and Component Annotation?**

<details><summary>Answer</summary>

> Bean annotation is used to return a specific instance. So we can have multiple methods that return a different object of the same class using 
> the bean annotation. Component annotation tells spring to create an instance of that class to be injected. 
> So Spring creates the object itself for component while with bean we have more control.
> 

</details>

**Q7 Explain Bean Annotation?**

<details><summary>Answer</summary>

> Specifies a factory method that is declared inside a config class. 
> This method returns a new instance of a spring bean that is then injected by the spring application.

</details>

**Q8 Explain Autowired Annotation?**

<details><summary>Answer</summary>

> Marks a dependency To be injected with a spring bean.

</details>

**Q9 Explain Qualifier Annotation?**

<details><summary>Answer</summary>

> The qualifier annotation is used alongside the autowired annotation. 
> It is used to specify a specific bean to be injected if there is any ambiguity. 
> For example if multiple beans are created in @configuration class then we need to specify which bean to inject.

</details>

**Q10 Explain Required Annotation?**

<details><summary>Answer</summary>

> Means the value of the dependency will be set through xml

</details>


**Q11 Explain Value Annotation?**

<details><summary>Answer</summary>

> Used to inject property values in beans. Or refer to property values in a .properties or yaml file

</details>


**Q12 Explain DependsOn Annotation?**

<details><summary>Answer</summary>

> Used to specify dependencies that are implicit dependencies eg jdbc driver loading. 
> The annotation is used on the class or the @bean annotation.

</details>


**Q13 Explain Lazy Annotation?**

<details><summary>Answer</summary>

> By Default beans are loaded eagerly at application startup. The lazy annotation is used to specify that this bean is only loaded when requested. 
> If this annotation is at the class level in a config class then all beans defined therein are lazy loaded. 
> If at an autowired annotation then that field is lazy loaded.

</details>



**Q14 Explain Lookup Annotation?**

<details><summary>Answer</summary>

> Used to inject a bean that has prototype scope into a bean than is singleton type scope.
</details>

**Q15 Explain Profile Annotation?**

<details><summary>Answer</summary>

>  If you want a spring bean to be active only when a certain profile is active you use the profile annotation

</details>


**Q16 Explain Import Annotation?**

<details><summary>Answer</summary>

>  Is used to import a config file if it is not annotated with @configuration

</details>



**Q17 Explain ImportResource Annotation?**

<details><summary>Answer</summary>

>  Used to import an xml resource to a config class

</details>


**Q18  Explain PropertySource Annotation?**

<details><summary>Answer</summary>

>  Defines property files for application settings. Also there is @PropertySources for multiple settings files

</details>



**Q19  Explain Transactional Annotation?**

<details><summary>Answer</summary>

>  When placed on method then all actions in method must be completed or everything is rolled back if there is an exception

</details>

**Q20 Can there be Nested Transaction?**

<details><summary>Answer</summary>

>  Yes. the transaction would be rolled back to after the inner transaction. eg to log data to log table

</details>

**Q21 What is ASOP**

<details><summary>Answer</summary>

>  AOP is aspect oriented programming. And is a way to increase modularity by allowing the separation of cross-cutting concerns. It avoids code duplication

</details>


**Q22 What does your current application do to connect to DB?**

<details><summary>Answer</summary>

> ### JDBC
> I have experience working on JDBC. I used JDBC to connect to legacy DB called NOLS
> I used JDBCTemplate that takes sql string and returns a resultset. 
> 
> ### JPA and QUERYDSL
> For connecting to MS SQL Server I used JPA/Hibernate and queryDSL for more expressive query like predicates
> 
> ### Liquibase
> I have experience with Liquibase which we use to deploy DB config changes 

</details>

**Q23 What is Cross-cutting concern**

<details><summary>Answer</summary>

> ### Concern
> A concern refers to a system divided on the basis of functionality. This may also be called primary concerns
> ### Cross-cutting concern
> A concern that applies to all other concerns. for example logging and security need to be applied to all other concerns. And may lead to code duplication.
> 

</details>

**Q24 How to make a class Immutable**

<details><summary>Answer</summary>

> Immutable class in java means that once an object is created, we cannot change its content. In Java, all the wrapper classes (like Integer, Boolean, Byte, Short) and String class is immutable. We can create our own immutable class as well.
> We need to make class final so that child classes can’t be created. And make private and final data members in class

</details>




**Q25 Explain Rest request params in Spring? And Mappings**
<details><summary>Answer</summary>

> @RequestParam vs @PathVariable Annotations
> @PathVariable annotation extracts value from url
>
> @RequestParam annotation extracts value from after the question mark

</details>



**Q26 What is the difference with singleton pattern and singleton bean in spring?**

<details><summary>Answer</summary>

> A singleton is reusing an object if it already exists otherwise create a new one. So only one object exists
> In Java a singleton is a singleton in the context of the class loader which loaded it. 
> In Spring, a singleton is per application context. 
>

</details>


**Q27 What is caching differences between soap and rest?**

<details><summary>Answer</summary>

> Soap messages are larger and more complex which makes them slower to transmit and process. Rest is faster and more efficient and also are cacheable, so the server can store frequently assessed data in a cache for shorter page load times. 

</details>



</details>

## 1.3 Spring Boot Questions



<details><summary>Spring Boot Questions</summary>

### 1.3.1 Question 1
**Q1 What is Spring Boot?**
<details><summary>Answer</summary>

>  Spring boot = Spring MVC + Auto Configuration(Don't need to write spring.xml file for configurations) + Server(You can have embedded Tomcat, Netty, Jetty server).

    And Spring Boot is an Opinionated framework, so its build taking in consideration for fast development, less time need for configuration and have a very good community support.

</details>


### 1.3.2 Question 2
**Q2 Global Exception Handling in Spring Boot**
<details><summary>Answer</summary>

> The Annotation @ControllerAdvice is used to handle exceptions globally in Spring Boot
> The annotation goes on the class definition and annotation @ExceptionHandler to a method of that class that
> defines how to handle a specific exception type.

>

</details>

### 1.3.3 Question 3
**Q3 Secure Rest Api Spring boot?**
<details><summary>Answer</summary>

> In spring, you can configure the content security policy by creating a bean that returns a SecurityFilterChain object. 
> 
> For http endpoint you can use secured annotation to specify roles that have access.
> 
</details>



</details>


## 1.4 Http Methods

<details><summary>Http Methods</summary>

**Q1 What is Idempotent?**
<details><summary>Answer</summary>

> From the point of view of rest service when Clients make the same call multiple times it should produce the same result

</details>



**Q2 What Http method is in theory not Idempotent?**
<details><summary>Answer</summary>

> Post as resource can already be created. 

</details>

**Q3 What are the Http methods**
<details><summary>Answer</summary>

> #### GET
> The GET method requests a representation of the specified resource. Requests using GET should only retrieve data.

> #### HEAD
> The HEAD method asks for a response identical to a GET request, but without the response body.

> #### POST
> The POST method submits an entity to the specified resource, often causing a change in state or side effects on the server.

> #### PUT
> The PUT method replaces all current representations of the target resource with the request payload.

> #### DELETE
> The DELETE method deletes the specified resource.

> #### CONNECT
> The CONNECT method establishes a tunnel to the server identified by the target resource.

> #### OPTIONS
> The OPTIONS method describes the communication options for the target resource.

> #### TRACE
> The TRACE method performs a message loop-back test along the path to the target resource.

> #### PATCH
> The PATCH method applies partial modifications to a resource.

</details>

</details>


## 1.5 Mockito/JUnit

<details><summary>Mockito/JUnit</summary>

[mockito-interview-questions](https://www.softwaretestinghelp.com/mockito-inteview-questions/)


**Q1 What is the difference between Junit and Mockito**

<details><summary>Answer</summary>

> JUnit is the Java library used to write tests (offers support for running tests and different extra helpers - like setup and teardown methods, test sets etc.). 
> Mockito is a library that enables writing tests using the mocking approach. JUnit is used to test API's in source code

</details>


**Q2 Why do we need mocking?**

<details><summary>Answer</summary>

> It is better to mock changes of state such as db calls.

</details>

**Q3 Difference between doReturn().when() and when().thenReturn()?**

<details><summary>Answer</summary>

> #### doReturn().when()
> Never calls the method specified in when. It just returns the value when the method should be called.
> 
> #### when().thenReturn() 
> The method that is mocked is called.

</details>

**Q4 What is a spy in Mockito?**

<details><summary>Answer</summary>

>  A spy wraps a real instance of the object that is being tested. When a method of the class is called then the real method is called. It is mostly used for partial mocks.

</details>



**Q5 What’s the need to verify that the mock was called?**

<details><summary>Answer</summary>

> Setting up a stub on a mocked object (or a spied instance) does not guarantee whether the stubbed setup was even invoked.
> “verification” matchers, give a facility to validate whether the stub that was set up was actually invoked or not, 
> how many times was the call made, what arguments were the stubbed method called with, etc. 
> In essence, it allows us to verify the test setup and expected outcome in a more robust manner.

</details>

**Q6  What are the limitations of Mockito?**
<details><summary>Answer</summary>

> Its inability to mock static methods.
> Constructors, private methods and final classes cannot be mocked.


</details>

</details>

## 1.6 Angular

<details><summary>Angular</summary>

[Angular Interview Questions & Answers](https://github.com/sudheerj/angular-interview-questions#what-is-angular-framework)


**Q1  What is Angular Framework?**

<details><summary>Answer</summary>

> Angular is a TypeScript-based open-source front-end platform that makes it easy to build applications with in web/mobile/desktop. 
> The major features of this framework such as declarative templates, dependency injection, end to end tooling, 
> and many more other features are used to ease the development.


</details>
</details>

## 1.7 Microservices

<details><summary>Microservices</summary>



**Q1  Microservices vs Monolith?**

<details><summary>Answer</summary>

> In software development, monolithic architecture is a traditional approach where all the software components are interdependent and use one code base to perform multiple business functions. This makes it restrictive and time-consuming to modify the architecture as small changes impact large areas of the code base. On the other hand, microservices architecture is an approach that composes software into small independent components or services. Each service performs a single function and communicates with other services through a well-defined interface. Because they run independently, you can update, modify, deploy, or scale each service as required
> 
> Here are some key differences between the two architectures:
>
Monolithic Architecture |	Microservices Architecture
---|--- 
Consists of a client-side UI, a database, and a server-side application | Each microservice works to accomplish a single feature or business logic
Developers build all of these modules on a single code base | Microservices communicate with an API
Small changes impact large areas of the code base  | Each service performs a single function
Easier to start with, as not much up-front planning is required | Requires more planning and design before starting
Deploying monolithic applications is more straightforward | Deploying microservice-based applications is more complex
> 
> While monolithic architecture is simpler to develop, microservices architecture is more scalable and flexible. However, it requires more planning and design before starting

</details>

</details>

## 1.9 Davy Questions Before Interview?

<details><summary> Davy Questions </summary>

**Q1 What framework I have used in my previous role. What do I know about Spring framework?**
<details><summary>Answer</summary>

</details>

**Q2 What Java 8 Features I have used?**
<details><summary>Answer</summary>

</details>

**Q3 What is Optional? How do I use it?**
<details><summary>Answer</summary>

</details>

**Q4 What is an abstract class?**
<details><summary>Answer</summary>

</details>


**Q5 Difference between Set and List? How can you sort the objects in the list?**
<details><summary>Answer</summary>

</details>

**Q6 What is difference between Hashmap and HashTable? How do they work internally? What happens if we use Object as a key in Hashmap?**
<details><summary>Answer</summary>

</details>

**Q7 How can you defend web application attacks like sql injection, XSS etc? Also what if client side validation is turned off?**
<details><summary>Answer</summary>

> Sql injection is when unvalidated input is used to make sql statements. or sql parameters. This can be prevented using prepared statements with the question mark placeholder (“?”) in our queries whenever we need to insert a user-supplied value. Use of whitelists which specifies allowed values of user input

> To prevent attacks like XSS clean user input. In a Spring web application, the user’s input is an HTTP request. To prevent the attack, we should check the HTTP request’s content and remove anything that might be executable by the server or in the browser.
>
> For a regular web application, accessed through a web browser, we can use Spring Security‘s built-in features (Reflected XSS). We can return a Content-Security-Policy header by providing a SecurityFilterChain bean

</details>

</details>

## 1.10 Spring Security

<details><summary> Spring Security Questions </summary>


**Q1 What is Spring security authentication and authorization?**
<details><summary>Answer</summary>

>Authentication: This refers to the process of verifying the identity of the user, using the credentials provided when accessing certain restricted resources. Two steps are involved in authenticating a user, namely identification and verification. An example is logging into a website with a username and a password. This is like answering the question Who are you?  
> 
> Authorization: It is the ability to determine a user's authority to perform an action or to view data, assuming they have successfully logged in. This ensures that users can only access the parts of a resource that they are authorized to access. It could be thought of as an answer to the question Can a user do/read this?

</details>


**Q2 What do you mean by basic authentication?**
<details><summary>Answer</summary>

> RESTful web services can be authenticated in many ways, but the most basic one is basic authentication. For basic authentication, we send a username and password using the HTTP [Authorization] header to enable us to access the resource. Usernames and passwords are encoded using base64 encoding (not encryption) in Basic Authentication. The encoding is not secure since it can be easily decoded.

</details>


**Q3 What do you mean by digest authentication?**
<details><summary>Answer</summary>

> RESTful web services can be authenticated in many ways, but advanced authentication methods include digest authentication. It applies a hash function to username, password, HTTP method, and URI in order to send credentials in encrypted form. It generates more complex cryptographic results by using the hashing technique which is not easy to decode.

</details>

**Q4 What do you mean by session management in Spring Security?**
<details><summary>Answer</summary>

> As far as security is concerned, session management relates to securing and managing multiple users' sessions against their request. It facilitates secure interactions between a user and a service/application and pertains to a sequence of requests and responses associated with a particular user. Session Management is one of the most critical aspects of Spring security as if sessions are not managed properly, the security of data will suffer. To control HTTP sessions, Spring security uses the following options:

</details>

**Q5 Explain SecurityContext and SecurityContext Holder in Spring security?**
<details><summary>Answer</summary>

> There are two fundamental classes of Spring Security: SecurityContext and SecurityContextHolder.

> SecurityContext: In this, information/data about the currently authenticated user (also known as the principal) is stored. So, in order to obtain a username or any other information about the user, you must first obtain the SecurityContext.

> SecurityContextHolder: Retrieving the currently authenticated principal is easiest via a static call to the SecurityContextHolder. As a helper class, it provides access to the security context. By default, it uses a ThreadLocal object to store SecurityContext, so SecurityContext is always accessible to methods in the same thread of execution, even if SecurityContext isn't passed around.

</details>

</details>

## 1.11 Database, JPA, Hibernate
<details><summary>Database</summary>

### 1.11.1 How would you use Explain Plans to improve performance

<details><summary>Answer</summary>

> An Explain plan can tell you if a query is causing a full table scan. Also if it should be using an index but it is not. Then you can create an index or fix an index to improve performance

</details>

### 1.11.2 Pitfalls to avoid when using JPA, Hibernate

<details><summary>Answer</summary>

>https://medium.com/@majbahbuet08/performance-pitfalls-while-using-spring-data-jpa-and-solutions-to-avoid-them-5eb4ee3fe4ea 

</details>

### 1.11.3 What is the difference between JOIN and JOIN FETCH

<details><summary>Answer</summary>

### Join
Given this JQPL Query

    FROM Employee emp
    JOIN emp.department dep 

Hibernate creates the following query

    SELECT emp.*
    FROM employee emp
    JOIN department dep ON emp.department_id = dep.id

### Join Fetch
Given The following JPQL Query

    FROM Employee emp
    JOIN FETCH emp.department dep

Hibernate creates the following query

    SELECT emp.*, dept.*
    FROM employee emp
    JOIN department dep ON emp.department_id = dep.id

</details>

### 1.11.4 Q4 JPA  Main Annotations
<details><summary>Answer</summary>

> @Entity
> Declares class as JPA entity so that jpa is aware of class and that the class represents a table.
>
> @Table
> This annotation tells JPA that the table could be a different name than the class name using @Table(name=""")
>
> @Transient
> Makes field non-persistent. It is not saved in DB

</details>

### 1.11.5 Q5 How to prevent leaks when accessing db in Spring?
<details><summary>Answer</summary>

> Use connection pool to manage db connection. Also make sure to close every connection in code in finally block and or try with resources.
</details>

### 1.11.6 What is a Transaction?

<details><summary>Answer</summary>

> **Transaction**
>
> Multiple actions that need to all complete or none of them

</details>


### 1.11.7 Q7 What are detached, persistent and transient objects in hibernate?


<details><summary>Answer</summary>

> **Transient**
>
> A Java Object that has no representation in DB
>
> **Persistent**
>
> Java object that does have a representation in DB is persistent. Because it has been associated with a session
>
> **Detached**
>
> Java object whose session has been closed has become detached as any new changes with not be reflected in the database

</details>
</details>


## 1.12 Software Design

<details><summary>Software Design/Architecture Questions  </summary>

### 1.12.1 Q1 How do you scale up an application, when you have more users and more traffic

<details><summary>Answer</summary>

> It depends on where the bottlenecks are and depends on the design and architecture of the system. A highly scalable application is one where if you need to handle 10x the request throughput, you can just run 10x the application instances on 10x the hardware. And if your application is stateless, this will probably be true regardless of the language you write code in. 
> On the other hand, if you build a system that's backed by a single relational database server, then that DB server will eventually become a scalability bottleneck. The trick is to identify which bottlenecks are actually relevant to your use case, and find practical ways of either removing them or working around them without over-engineering.
> 
> When your code is blocking you will have to scale vertically. When the code is asynchronous with a bunch of microservices you will be able to scale horizontally

> ### Scale Up 
> add more ram, faster and/or more cpu, faster/more disk (in otherwords, add more hardware capacity to what you already have)
> 
> ### Scale Out  
> add more instances (depending on your runtime this may be more physical servers, more virtual servers, more runtime containers etc)

> Scaling up usually reaches constraints pretty quick, e.g. there's only so much ram a server can support before you max out what's possible), scaling out depends greatly on how your app is designed and how much gain this can get you (e.g. some decisions like shared state between instances drastically impact your ability to scale out)
> 
> https://github.com/donnemartin/system-design-primer?tab=readme-ov-file#system-design-topics-start-here

</details>

### 1.12.2 Q2 Explain Vertical Scaling

<details><summary>Answer</summary>

> Vertical scaling is adding more CPU, RAM to a system. By adding more power to a single node of a system.
> The same code can run on a higher spec device. Also improve performance of code.
> Benefit is lower latency between different parts of network. Drawback is single point of failure and cost too much after upgrading to a certain point.

</details>

### 1.12.3 Q3 Explain Horizontal Scaling

<details><summary>Answer</summary>

> By adding more Nodes for virtual machines to a system to handle more demand. 
> This requires modifying a sequential piece of logic in order to run workloads concurrently on multiple machines.
> Two major forms of horizontal scaling is database scaling and compute scaling.
> 
> **Database Scaling - Replication**
> 
> Have multiple databases that are replicas of each other. For example have database servers in different regions closer to users.
> 
> **Database Sharding**
> 
> Data is partitioned across multiple servers. Each shard holds a subset of the dataset. each shard manages a different piece of the data.
> 
> **Compute Scaling**
>
> Adding more instances or nodes to split up larger jobs into a series of smaller ones. 

</details>

### 1.12.4 Q4 Explain Caching

<details><summary>Answer</summary>

> https://saikris12.medium.com/spring-boot-2-with-ehcache-3-jcache-using-h2-database-f47bb566b2a0
> Caching is used to cache results of database requests to optimize performance by reducing unnecessary calls to database. Ehcache or Jcache is used with Spring. In xml configurations file you can create an alias that in Spring you reference using @Cacheable annotation.

</details>

### 1.12.5 Q5 Explain Load Balancing

<details><summary>Answer</summary>

>  Required for horizontal scaling.
> 

</details>

### 1.12.6 Q6 What is Scalability?

<details><summary>Answer</summary>

>Scalability refers to a system’s ability to handle increased load by adding resources. As your user base grows or traffic spikes, you want your system to maintain its performance without downtime or degradation. Scalability ensures that the system can grow and continue functioning efficiently.
> 
> https://medium.com/@yashpaliwal42/scalability-and-load-balancing-the-backbone-of-modern-system-design-8444619f8745


</details>

### 1.12.7 Q7 Explain CAP Theorem

<details><summary>Answer</summary>

>  In Database theory the cap theorem states that any distributed data store can provide only two of the following three guarantees. Consistency, Availability, Partition tolerance.
> 
#### **Consistency** 
> Means every node in the network will have access to the same data
>
#### **Availability**
> Means that the system is always available to the users 
> 
#### **Partition Tolerance**
> Means that in case of a fault in the network or communication, the system will still work.
> 
> 
</details>

### 1.12.8 Q8 Explain Web authentication and basic security
<details><summary>Answer</summary>

> https://interviewing.io/guides/system-design-interview/part-two#concepts-authentication

</details>


### 1.12.9 Q9 Describe what you would do to add a new Endpoint to a system?

<details><summary>Answer</summary>

</details>
</details>

