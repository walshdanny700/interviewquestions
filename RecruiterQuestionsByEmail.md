# Recruiter Questions

## Q1. Can you describe your experience working with Java and React? How have you applied these technologies in previous projects?

> I have worked with Java, Spring framework and Spring Boot in my last two companies. In my previous company I started working on Java developing Rest endpoints that was consumed by multiple applications. The most recent project I worked on in my current company is migrating a service from Java 8 to Java 17. I have worked with React for the last two years, migrating to React from legacy code and using webpack and Jest.
> 

## Q2. Have you worked on cloud-based applications or solutions? Can you share an example of a project where you utilized cloud development techniques or platforms?

> I was involved in migrating multiple applications that our team supports to AWS. I was responsible for writing the Ansible code to deploy applications to EC2 instances running both linux and windows. 
> 

## Q3. Do you have experience working with microservices architecture and tools like Maven, Jenkins, Jira, or Confluence?

> I have experience working on and supporting multiple applications and services, including micro services. I have experience using build tools like Maven and also Gradle and Jenkins. I also use Jira and Confluence every day.


## Q4. This is a hybrid position based in Dublin, requiring two days in the office each week. Would this arrangement work for you?

> Yes, Two days in the office in Dublin works for me.
