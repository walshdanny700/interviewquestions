# 1 Javascript

<details><summary>Javascript</summary>


**1.1  Javascript is it multithreaded?**

<details><summary>Answer</summary>

> Javascript is Not a multi-threaded language. It is event-driven and single-threaded. This means that Javascript code is executed in a single thread, and all events are queued in a single queue. The event loop processes the queue one event at a time, and each event is processed to completion before the next event is processed.
>
> Although Javascript is not multi-threaded, it is possible to use Web Workers to run scripts in the background, which can help improve performance. Web Workers are a way to run scripts in the background, without blocking the main thread. They allow you to run scripts in parallel, which can help improve performance for CPU-intensive tasks.
</details>

**1.2 Browser how does the browser handled async behind the scenes?**

<details><summary>Answer</summary>

> Asynchronous programming is a popular technique used in JavaScript to handle long-running tasks without blocking the main thread. In JavaScript, asynchronous operations are handled by the browser using a combination of single-threaded, non-blocking, and asynchronous features 1.
>
> When a programming language is referred to as single-threaded, it means the language can execute only one instruction at a time. This differs from multi-threaded programming languages that run multiple instructions at once. When a programming language is said to be non-blocking, it means that the language does not wait for a specific previous instruction to finish executing before it moves to the next one. This ensures that no instruction blocks or obstructs the execution of subsequent instructions. If a programming language is not non-blocking, it could lead to slow applications. JavaScript is also asynchronous (async), which means that it can handle a large number of tasks at a time. This is a feature of multi-threaded programming languages, but JavaScript achieves it with a single thread 1.
>
> To handle asynchronous operations, JavaScript uses the browser’s provided Web Application Programming Interfaces (Web APIs) 1. Whenever JavaScript encounters asynchronous instructions like requests to third-party sites, or timer-based actions, it seeks assistance from the browser’s Web APIs. These APIs are responsible for handling the asynchronous tasks and returning the results to JavaScript when they are ready 1.


</details>

</details>
