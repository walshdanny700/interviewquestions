# 1. Recruiter Screening

https://interviewing.io/blog/sabotage-salary-negotiation-before-even-start

## 1.1 Start of process - What are your salary expectations?

<details><summary>Start of Process - What are your salary expectations?</summary>

> At this point, I don’t feel equipped to throw out a number because I’d like to find out more about the opportunity first – right now, I simply don’t have the data to be able to say something concrete. If you end up making me an offer, I would be more than happy to consider it if needed and figure out something that works. I promise not to accept other offers until I have a chance to discuss them with you.
> 

</details>


## 1.2 Start Of Process - Where else are you interviewing?

<details><summary>Start Of Process - Where else are you interviewing?</summary>

> I’m currently speaking with a few other companies and am at various stages with them. I’ll let you know if I get to the point where I have an exploding offer, and I promise not to accept other offers until I have a chance to discuss them with you.

</details>

## 1.3 Start Of Process - Recruiter gives a salary range

<details><summary>Start Of Process - Recruiter gives a salary range</summary>

> Thank you for sharing that with me. Right now I don’t know enough about the opportunity to value it concretely, and I honestly haven't done my market research. If you end up making me an offer, I would be more than happy to iterate on it if needed and figure out something that works. I promise not to accept other offers until I have a chance to discuss them with you.

</details>
