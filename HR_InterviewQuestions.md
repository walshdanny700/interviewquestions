# 1. HR Interview Questions

https://interviewing.io/blog/how-software-engineering-behavioral-interviews-are-evaluated-meta

## 1.1 Conflict Resolution and Empathy

<details><summary>Example Questions</summary>

> **Example Questions**
> 
> “Tell me about a person or team who you found most challenging to work with.”
> 
> “Tell me about a time you disagreed with a coworker.”
> 
> “Tell me about a situation where two teams couldn’t agree on a path forward.”


> **Example Responses**
> 
> Junior: A story about how they were able to work through a disagreement with a coworker on an implementation detail of a larger project.
>
> Senior: A story about how they were able to work through a disagreement with a few coworkers or team leads on the direction of a larger project.
>
> Staff: A story about how they were able to work through a disagreement with two or more teams on the direction of a large project.


> **My Answer**
> 
> As Junior engineer Learned to be more assertive when giving accurate estimates.
> 
> Conflict with AWS transition, Schuber philis naivety, timelines,   

</details>
